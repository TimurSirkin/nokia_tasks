#include "tracker.h"

void test1(){
    Tracker tracker;

    size_t inX, inY, tgX, tgY;

    std:: cout << "Table:\n";
    tracker.setTable();
    std::cout << "\nStar cell\n";
    std::cout << "x = ";
    std::cin >> inX;
    std::cout << "y = ";
    std::cin >> inY;
    std::cout << "Target cell\n";
    std::cout << "x = ";
    std::cin >> tgX;
    std::cout << "y = ";
    std::cin >> tgY;
    int result =  tracker.getTrack(inX, inY, tgX, tgY);
    std:: cout << "\n\nAnswer:\n" << result << "\n\n";
};

void test2(){
    const size_t rowCount = 3;
    const size_t columnCount = 3;
    Tracker tracker;

    Table table = new bool*[rowCount];
    for (size_t row = 0; row < rowCount; row++){
            table[row] = new bool[columnCount];
    }

    table[0][0] = 0; table[0][1] = 0; table[0][2] = 0;
    table[1][0] = 1; table[1][1] = 1; table[1][2] = 0;
    table[2][0] = 0; table[2][1] = 0; table[2][2] = 0;

    std:: cout << "Table:\n";
    tracker.setTable(table, rowCount, columnCount);
    tracker.showTable(table);
    int result =  tracker.getTrack(0, 0, 2, 0);
    std:: cout << "\nAnswer:\n" << result << "\n\n";
}

int main()
{
    try {
        test1();
    } catch (std::exception &err) {
        std::cout << err.what();
    }
    return 0;
}
