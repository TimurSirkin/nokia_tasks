#include "tracker.h"

Tracker::~Tracker()
{
    deleteTable(_table);
}

void Tracker::setTable()
{
    unsigned int rowCount;
    std::cout << "Row count = ";
    std::cin >> rowCount;

    unsigned int columnCount;
    std::cout << "Column count = ";
    std::cin >> columnCount;

    deleteTable(_table);
    createTable(rowCount, columnCount);

    std::cout << "Enter table elements (only 0 or 1)\n";
    for (size_t row = 0; row < _rowCount; row++){
        for (size_t column = 0; column < _columnCount; column++){
             std::cin >> _table[row][column];
        }
    }
}

void Tracker::setTable(Table table, size_t rowCount, size_t columnCount)
{
    deleteTable(_table);
    _table = table;
    _rowCount = rowCount;
    _columnCount = columnCount;
}

void Tracker::showTable(Table table) const
{
    for (size_t row = 0; row < _rowCount; row++){
        for (size_t column = 0; column < _columnCount; column++){
            std::cout << "\t[" << table[row][column] << "]";
        }
        std::cout << "\n";
    }
}

int Tracker::getTrack(size_t inX, size_t inY, size_t tgX, size_t tgY)
{
    // Check outbound of table
    if ( inX >= _rowCount || inY >= _columnCount){
        throw std::logic_error("Start cell outside the table");
    }
    if ( tgX >= _rowCount || tgY >= _columnCount){
        throw std::logic_error("Target cell outside the table");
    }


    // Check if start or target cells are blocked
    if (_table[inX][inY] == BLOCK){
        throw std::logic_error("Start cell blocked");
    } else if (_table[tgX][tgY] == BLOCK){
        throw std::logic_error("Target cell blocked");
    }

    Cell target = Cell(tgX, tgY); // Target cell
    if (target == Cell(inX, inY)){
        std::cout << "Start cell equal to target cell\n";
        return 0;
    }

    // Make copy of _table
    // In the copy we will mark the cells in which we could get
    Table table = new bool*[_rowCount];
    for (size_t row = 0; row < _rowCount; row++){
        table[row] = new bool[_columnCount];
         for (size_t column = 0; column < _columnCount; column++){
             table[row][column] = _table[row][column];
         }
    }

    int stepCount = 1; // Step counter

    Cells cells = getValidCells(Cell(inX, inY), table); // Get cells that we can get into in 1 step
    bool isValidCellExist = !cells.empty(); // Check if there are any

    while (isValidCellExist){ // While we can move somewhere
        for (const Cell &cell : cells){ // Block cells from previous step
            table[cell.x][cell.y] = BLOCK;
        }
        Cells newCells; // Valid cells of this step

        for (const Cell &cell : cells){
            // If reached target cell
            if (cell == target){
                deleteTable(table);
                return stepCount;
            }
            // Collect all valid cells of this step into one list
            Cells validCells = getValidCells(cell, table);
            newCells.insert(validCells.begin(), validCells.end());
        }

        stepCount++;
        cells = newCells;
        isValidCellExist = !cells.empty();
    }


    std::cout << "It is not possible to hit the target cell\n";
    deleteTable(table);
    return -1;
}

void Tracker::deleteTable(Table table)
{
    if (_rowCount == 0 || _columnCount == 0){
        return;
    }
    for (size_t row = 0; row < _rowCount; row++){
        delete [] table[row];
    }
    delete [] table;
}

void Tracker::createTable(size_t row, size_t column)
{
    _rowCount = row;
    _columnCount = column;
    _table = new bool*[row];
    for (size_t row = 0; row < _rowCount; row++){
        _table[row] = new bool[column];
    }
}

Cells Tracker::getValidCells(const Cell &cell, Table table)
{
    Cells cells; // Result list, contains all valid cells

    table[cell.x][cell.y] = BLOCK; // Block specified cell

    // X axis down
    for (int x = cell.x + 1; x < _rowCount; x++){ // Until we get to the border
        if (table[x][cell.y] == FREE){ // If cell is free
            cells.emplace(x, cell.y); // Add it in list
        }else{ // If cell is blocked - try to move by other axis
            break;
        }
    }

    // X axis up
    for (int x = cell.x - 1; x >= 0; x--){
        if (table[x][cell.y] == FREE){
            cells.emplace(x, cell.y);
        }else{
            break;
        }
    }

    // Y axis down
    for (int y = cell.y + 1; y < _rowCount; y++){
        if (table[cell.x][y]  == FREE){
            cells.emplace(cell.x,y);
        }else{
            break;
        }
    }

    // Y axis up
    for (int y = cell.y - 1; y >= 0; y--){
        if (table[cell.x][y]  == FREE){
            cells.emplace(cell.x,y);
        }else{
            break;
        }
    }

    return cells;
}

bool Cell::operator==(const Cell &cell) const{
    return ((cell.x == x) && (cell.y == y));
}

bool Cell::operator<(const Cell &cell) const {
    if (x != cell.x){
        return (x < cell.x);
    } else {
        return (y < cell.y);
    }
}
