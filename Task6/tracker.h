#ifndef TRACKER_H
#define TRACKER_H

#include <iostream>
#include <list>
#include <set>

#define Table bool **
#define Cells std::set<Cell>
#define BLOCK true
#define FREE false

// Contains the x and y coordinates of the table
struct Cell{
    Cell(size_t _x, size_t _y){
        x = _x;
        y = _y;
    }
    size_t x;
    size_t y;

    bool operator== (const Cell &cell) const;
    bool operator< (const Cell &cell) const;
};

class Tracker
{
public:
    Tracker() = default;
    ~Tracker();

    void setTable(); // Set table from console
    void setTable(Table table, size_t rowCount, size_t columnCount); // Set table by array
    void showTable(Table table) const; // Prints the table to the console

    int getTrack(size_t inX, size_t inY, size_t tgX, size_t tgY); // Get the minimum number of steps
                                                                    // to get from (inX,inY) to (tgX, tgY)
private:
    Table _table; // Each cell contains 0 (free cell) or 1 (blocked cell)
    size_t _rowCount = 0; // Number of table rows
    size_t _columnCount = 0; // Number of table columns

    void deleteTable(Table table); // Delete the specified table
    void createTable(size_t row, size_t column); // Create bool two-dimensional array for _table

    Cells getValidCells(const Cell& cell, Table table); // Get valid cells that can be reached from the specified one
                                                        // At the same time, block all such cells in the specified table
};

#endif // TRACKER_H