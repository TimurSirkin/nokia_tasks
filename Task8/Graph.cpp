//
// Created by timur on 26.09.2019.
//

#include "Graph.h"

void Graph::randomize(size_t edgeCount) {
    deleteMatrix();
    createMatrix(edgeCount);

    srand(time(nullptr));
    unsigned int connectCount = 0; // Connected edges counter
    bool isChecked = false; // Is edge checked on connection
    for (size_t row = 0; row < _edgeCount; row++){
        for (size_t column = 0; column < _edgeCount; column++){
            bool value = rand() % 2; // Get 0 or 1
            _matrix[row][column] = value;
            if (!isChecked  && value && row != column){ // If edge is not checked and have connection with other edge
                    connectCount++;
                    isChecked = true;
            }
        }
        isChecked = false;
    }
    _connectCount = connectCount;
}

void Graph::deleteMatrix() {
    if (isEmpty()){
        return;
    }
    for (size_t row = 0; row < _edgeCount; row++){
        delete [] _matrix[row];
    }
    delete [] _matrix;
}

void Graph::createMatrix(size_t edgeCount)
{
    _edgeCount = edgeCount;
    _matrix = new bool*[edgeCount];
    for (size_t row = 0; row < edgeCount; row++){
        _matrix[row] = new bool[edgeCount];
    }
}

bool Graph::isEmpty() {
    return _edgeCount == 0;
}

std::ostream &operator<<(std::ostream &out, const Graph &graph) {
    for (size_t row = 0; row < graph._edgeCount; row++){
        for (size_t column = 0; column < graph._edgeCount; column++){
            std::cout << "\t[" << graph._matrix[row][column] << "]";
        }
        std::cout << "\n";
    }
}

unsigned int Graph::getConnectCount() {
    return _connectCount;
}
