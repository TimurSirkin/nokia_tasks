//
// Created by timur on 26.09.2019.
//

#ifndef GRAPH_GRAPH_H
#define GRAPH_GRAPH_H

#include <iostream>

class Graph {
public:
    void randomize(size_t edgeCount); // Fill adjacency matrix by random values (0 or 1)
    friend std::ostream& operator<< (std::ostream &out, const Graph &graph);
    unsigned int getConnectCount(); // Returns number of edges

    bool isEmpty(); // Is adjacency matrix empty
private:
    bool **_matrix; // Adjacency matrix
    size_t _edgeCount = 0; // Number of edges
    unsigned int _connectCount; // Number of connected edges connected with others
                                // (Edges connected to themselves are not considered)

    void deleteMatrix(); // Delete adjacency matrix
    void createMatrix(size_t edgeCount); // Create adjacency matrix
};


#endif //GRAPH_GRAPH_H
