#include "Graph.h"

int main() {
    Graph graph;
    for (size_t i = 1; i < 10; i++){
        graph.randomize(i);
        std::cout << "Adjacency matrix:\n";
        std::cout << graph;
        std::cout << "Connected edges count = " << graph.getConnectCount() << "\n\n";
    }
    return 0;
}